#!/bin/bash
# Script Licence: GNU/GPL v3
#   2017 - Eric Seigne <eric.seigne@abuledu.org>
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
#comme on est relais il faut qu'on sache quels sont les domaines virtuels qu'on heberge ...
#requete sur dolibarr pour avoir la liste ...
# https://sud-ouest2.org/dolibarr/public/soo/get_liste_domains.php
# et
# https://sud-ouest2.org/dolibarr/public/soo/get_liste_sympa.php
FIC=`tempfile`

LADATE=`date "+%Y%m%d %H:%M"`
echo "------------- ${LADATE}"

wget -q https://sud-ouest2.org/dolibarr/public/soo/get_liste_domains.php -O ${FIC}
if [ $? != 0 ]; then
    #erreur de transfert ou autre
    echo "erreur de transfert pour https://sud-ouest2.org/dolibarr/public/soo/get_liste_domains.php"
    exit
fi
wget -q https://sud-ouest2.org/dolibarr/public/soo/get_liste_sympa.php -O - >> ${FIC}
if [ $? != 0 ]; then
    #erreur de transfert ou autre
    echo "erreur de transfert pour https://sud-ouest2.org/dolibarr/public/soo/get_liste_sympa.php"
    exit
fi
#on ajoute les domaines de base de l'association
cat /etc/postfix/relaydomains.base >> ${FIC}

diff ${FIC} /etc/postfix/relaydomains.reference

#pour ne pas tout refaire si pas de nouveautes
diff ${FIC} /etc/postfix/relaydomains.reference
if [ $? == 0 ]; then
    #aucune modif -> exit
    echo "aucune modification depuis la derniere fois -> exit"
    rm ${FIC}
    exit
fi

echo "il y a des modification depuis la derniere fois ..."
rm /etc/postfix/relaydomains.reference
rm /etc/postfix/relaydomains
rm /etc/postfix/relaydomains.db

rm /etc/postfix/transport
rm /etc/postfix/transport.db

for domaine in `cat ${FIC}`
do
    echo "  $domaine [ajout]"
    echo "$domaine	OK" >> /etc/postfix/relaydomains
    echo "$domaine	smtp:mx-in-01.sud-ouest2.org" >> /etc/postfix/transport
done

cp ${FIC} /etc/postfix/relaydomains.reference

echo "construction de la base de type hash pour /etc/postfix/relaydomains"
/usr/sbin/postmap hash:/etc/postfix/relaydomains
chmod 644 /etc/postfix/relaydomains*

echo "construction de la base de type hash pour /etc/postfix/transport"
/usr/sbin/postmap hash:/etc/postfix/transport
chmod 644 /etc/postfix/transport*

rm ${FIC}

echo "restart de postfix ..."
service postfix restart
echo "the end"

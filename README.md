# Configuration et scripts pour le/les serveurs MX secondaires/backup

L'infrastructure de sud-ouest2.org est prévue pour avoir plusieurs serveurx MX entrant, vous trouverez donc dans ce dépot les fichiers de configuration et / ou les scripts utilisés pour le serveur MX secondaire/backup

Si le serveur MX frontal principal est en panne ou indisponible
* On lance le serveur MX secondaire/backup
* Il récupère tout et garde en local
* Quand le serveur principal est de nouveau en ligne il lui expédie tout ce qui est en attente
* Et enfin, on stoppe le serveur secondaire

## À lire

Après avoir lu et compris la documentation et avoir déjà mangé des tonnes de spams relayés par des serveurs MX secondaires et avoir bien lu l'article de Stéphane sur le conseil qu'il vaut mieux ne pas avoir de serveur MX secondaire … http://www.bortzmeyer.org/mx-secondaire.html je fais le choix d'en monter un qui sera éteint (postfix stop) et ne sera lancé (manuellement) que lorsque le serveur principal subira une panne de plus de 3 jours.

On devrait donc pouvoir faire face à une situation compliquée ... en tout cas cette infra est prévue pour.

## Installation OS

* OS de base : Debian GNU/Linux
* Paquets logiciels installés : voir le contenu du fichier dpkg.get_selections

## Configuration postfix
* Voir les fichiers présents dans le dépot

## Scripts personnalisés

Tous les scripts personnalisés sont dans /usr/local/bin :

* soo2-update_certs.sh et son cron associé mettent à jour les certificats SSL via let's encrypt
* soo2-update_relays.sh et son cron associé mettent à jour la liste des règles de relais

## Certificats Let's encrypt pour postfix

Pour que postfix soit accessible en ssl/tls (trafic serveur-serveur) nous avons mis en place un certificat Let's Encrypt.

* Voir /usr/local/bin/soo2-update_certs.sh et son cron associé
* Voir la configuration main.cf de postfix smtp_tls et smtpd_tls
